﻿var idEvento;

var agenda = {

    carregarParticipantesEvento: function (id) {
        $.ajax({
            cache: false,
            type: "GET",
            url: 'https://localhost:44306/api/agenda/GetParticipantes',
            data: { idEvento: id },
            dataType: 'json',
            success: function (data) {
                var select = $("#ddlParticipantesEvento");
                $.each(data, function (i, d) {
                    $('<option>').val(d.id).text(d.nome).appendTo(select);
                });
            },
            error: function (error) {
                alert("Erro ao carregar participantes");
            }
        });
    },

    carregarUsuarios: function () {
        $.ajax({
            cache: false,
            type: "GET",
            url: 'https://localhost:44306/api/agenda/GetParticipantes',
            dataType: 'json',
            success: function (data) {
                var select = $("#ddlParticipantesNovoEvento");
                $.each(data, function (i, d) {
                    $('<option>').val(d.id).text(d.nome).appendTo(select);
                });
            },
            error: function (error) {
                alert("Erro ao carregar participantes");
            }
        });
    },

    carregarTipoEvento: function () {
        $.ajax({
            cache: false,
            type: "GET",
            url: 'https://localhost:44306/api/agenda/GetTipoEvento',
            dataType: 'json',
            success: function (data) {
                var select = $("#ddlTipoEvento");
                $.each(data, function (i, d) {
                    $('<option>').val(d.tipoEventoId).text(d.descricao).appendTo(select);
                });
            },
            error: function (error) {
                alert("Erro ao carregar participantes");
            }
        });
    },


    ////Problema de CORS 
    ////
    salvarEvento: function () {

        var evento = {}
        evento.Title = $('#txtNomeNovoEvento').val();
        evento.Descricao = $('#txtDescricaoNovoEvento').val();
        evento.Local = $('#txtLocalNovoEvento').val();
        evento.Start = $('#txtDataInicioNovoEvento').val();
        evento.End = $('#txtDataFimNovoEvento').val();
        evento.TipoEvento = $('#ddlTipoEvento').val();

        $.ajax({
            type: "POST",
            url: 'https://localhost:44306/api/agenda/SalvarEvento',
            data: JSON.stringify(evento),
            contentType: "application/json; charset=utf-8",
            dataType: 'json',
            success: function (data) {
                alert("salvou o evento");
            },
            error: function (xhr, err) {
                console.log(xhr);
                console.log(err);
                alert("Erro ao salvar evento");
            }
        });
    },

    limparCampos: function (campo) {
        $(campo)
            .find("input,textarea")
            .val('')
            .end();
    }
};

document.addEventListener('DOMContentLoaded', function () {
    var calendarEl = document.getElementById('calendar');

    var calendar = new FullCalendar.Calendar(calendarEl, {
        locale: 'pt-br',
        handleWindowResize: true,
        plugins: ['interaction', 'dayGrid'],
        header: {
            left: 'addEventButton',
            center: 'title',
            right: 'today prev,next'
        },
        customButtons: {
            addEventButton: {
                text: 'Adicionar evento',
                click: function () {
                    $('#modalAdicionarEvento').modal('show');   
                }
            }
        },
        columnHeaderFormat: {
            hour12: 'false',
            weekday: 'long'
        },
        eventClick: function (info) {
            idEvento = info.event.id;
            $('#modalDetalhesEvento').modal('show');
            $('#txtNomeEvento').text(info.event.title);
            $('#txtDescricaoEvento').text(info.event.extendedProps.descricao);
            $('#txtLocalEvento').text(info.event.extendedProps.local);
            $('#txtDataInicio').text(info.event.start.toLocaleString());
            $('#txtDataFim').text(info.event.end.toLocaleString());
            $('#txtTipoEvento').text(info.event.extendedProps.tipoEvento);

            agenda.carregarParticipantesEvento(idEvento);
        },
        events: {
            url: 'https://localhost:44306/api/agenda/GetEventos',
            method: 'GET',
            extraParams: {
                user: '1'
            },
            color: 'yellow',
            textColor: 'black'
        }
    });

    calendar.render();
});

$(document).ready(function () {
    $('#modalAdicionarEvento').on('show.bs.modal', function () {
        agenda.carregarUsuarios();
        agenda.carregarTipoEvento();
    });

    $('#modalAdicionarEvento').on('hidden.bs.modal', function (e) {
        agenda.limparCampos('#modalAdicionarEvento');
        $("#ddlParticipantesNovoEvento").empty();
    });

    $('#modalDetalhesEvento').on('hidden.bs.modal', function (e) {
        agenda.limparCampos('#modalDetalhesEvento');
        $("#ddlParticipantesEvento").empty();
    });

    $('#btnSalvarEvento').click(function () {
        agenda.preencherDTO();
        agenda.salvarEvento();
    });
});
